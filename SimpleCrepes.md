# Simple Crepes

## Ingredients
- 1 cup unbleached all-purpose flour
- 1 tablespoon granulated sugar
- 1/4 teaspoon kosher salt
- 1 1/2 cups whole milk, room temperature
- 4 large eggs, room temperature
- 3 tablespoons unsalted butter, melted, plus more for brushing 

**Step 1**

In a blender, puree flour, sugar, salt, milk, eggs, and butter until smooth, about 30 seconds. Refrigerate for 30 minutes or up to 1 day; stir for a few seconds before using.

**Step 2**

Heat an 8-inch nonstick skillet over medium. Lightly coat with butter. Quickly pour 1/4 cup batter into center of skillet, tilting and swirling pan until batter evenly coats bottom. Cook until crepe is golden in places on bottom and edges begin to lift from pan, 1 to 1 1/2 minutes. Lift one edge of crepe with an offset spatula, then use your fingers to gently flip crepe. Cook on second side until just set and golden in places on bottom, about 45 seconds. Slide crepe onto a paper towel-lined plate.

**Step 3**

Repeat with remaining batter, coating pan with more butter as needed, and stacking crepes directly on top of one another. Let cool to room temperature before using, wrapping in plastic wrap and refrigerating up to 5 days, or freezing up to 1 month.

